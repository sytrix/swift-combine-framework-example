//
//  ViewController.swift
//  TestCombineFrameworkSwift
//
//  Created by Pierre Mazan on 22/08/2019.
//  Copyright © 2019 Pierre Mazan. All rights reserved.
//

import UIKit
import Combine

extension Notification.Name {
    static let newMessage = Notification.Name("newMessage");
}

struct Message {
    let content:String
    let author:String
}

class ViewController : UIViewController {

    let _textFieldA = UITextField()
    let _textFieldB = UITextField()
    let _button = UIButton(type:UIButton.ButtonType.custom)
    let _labelView = UILabel()
    let _font = UIFont(name:"Helvetica", size:25)
    let _fontB = UIFont(name:"Helvetica", size:12)
    
    @Published var stringOne:String? = ""
    private var stringSubscriber:AnyCancellable?
    
    //
    
    override func loadView() {
        super.loadView()
        
        _textFieldA.frame = CGRect(x:20, y:64, width:self.view.bounds.size.width - 40, height:45)
        _textFieldA.layer.borderColor = UIColor.red.cgColor
        _textFieldA.layer.borderWidth = 1
        _textFieldA.layer.cornerRadius = 3
        _textFieldA.font = _font
        _textFieldA.addTarget(self, action:#selector(textFieldDidChange), for:.editingChanged)
        self.view.addSubview(_textFieldA)
        
        _textFieldB.frame = CGRect(x:20, y:124, width:self.view.bounds.size.width - 40, height:45)
        _textFieldB.layer.borderColor = UIColor.red.cgColor
        _textFieldB.layer.borderWidth = 1
        _textFieldB.layer.cornerRadius = 3
        _textFieldB.font = _font
        self.view.addSubview(_textFieldB)
        
        _button.frame = CGRect(x:20, y:184, width:self.view.bounds.size.width / 4, height:45)
        _button.setTitle("send", for:UIControl.State.init())
        _button.setTitleColor(UIColor.black, for:UIControl.State.init());
        _button.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
        _button.addTarget(self, action:#selector(sendMessage(_:)), for:.touchDown)
        _button.layer.cornerRadius = 4;
        self.view.addSubview(_button)
        
        _labelView.frame = CGRect(x:20, y:244, width:self.view.bounds.size.width - 40, height:45)
        _labelView.font = _fontB
        _labelView.numberOfLines = 3
        self.view.addSubview(_labelView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        stringSubscriber = $stringOne.receive(on: DispatchQueue.main).assign(to: \.text, on:_textFieldB)
        
        let messagePublisher = NotificationCenter.Publisher(center: .default, name: .newMessage)
            .map { notification -> String? in
                return (notification.object as? Message)?.content ?? ""
        }
        let messageSubscriber = Subscribers.Assign(object: _labelView, keyPath: \.text)
        
        messagePublisher.subscribe(messageSubscriber)
    }
    
    @objc func textFieldDidChange() {
        stringOne = String(format:"Text [%ld] : %@", _textFieldA.text?.count ?? 0, _textFieldA.text ?? "")
    }
    
    @objc func sendMessage(_ sender: Any) {
        let messageContent:String? = _textFieldA.text
        let message = Message(content: "Message : \"\(messageContent ?? "")\" at time : \(Date())", author:"Pierre")
        NotificationCenter.default.post(name:.newMessage, object:message)
    }

}


